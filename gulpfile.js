var gulp = require('gulp'),
    gutil = require('gulp-util'),
    sourcemaps = require('gulp-sourcemaps'),
    plugins = require("gulp-load-plugins")({
        pattern: ['gulp-*', 'gulp.*', 'jshint-*'],
        replaceString: /\b(gulp|jshint)[\-.]/
    }),
    basePaths = {
        src: './source',
        dest: './public'
    },
    paths = {
        js: {
            src: basePaths.src + '/js',
            dest: basePaths.dest + '/js'
        },
        css: {
            src: basePaths.src + '/scss',
            dest: basePaths.dest + '/css'
        },
        images: {
            src: basePaths.src + '/img',
            dest: basePaths.dest + '/img'
        }
    };

gulp.task('styles', function () {
    return gulp.src(paths.css.src + '/**/*.scss')
        .pipe(plugins.plumber({
            errorHandler: function (error) {
                console.log('Styles Error: ' + error.message);
                this.emit('end');
            }
        }))
        .pipe(sourcemaps.init())
        .pipe(plugins.sass())
        .pipe(plugins.autoprefixer('last 2 version'))
        .pipe(plugins.minifyCss())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(paths.css.dest));
});

gulp.task('lint', function () {
    return gulp.src(paths.js.src + '/adland/**/*.js')
        .pipe(plugins.plumber({
            errorHandler: function (error) {
                console.log('Scripts Lint Error: ' + error.message);
                this.emit('end');
            }
        }))
        .pipe(plugins.jshint())
        .pipe(plugins.jshint.reporter(plugins.stylish))
        .pipe(plugins.jshint.reporter('fail'));
});

gulp.task('scripts', ['lint'], function () {
    return gulp.src(
        [
            paths.js.src + '/jquery-1.11.2.min.js',
            paths.js.src + '/plugins/**/*.js',
            paths.js.src + '/adland/**/*.js'
        ])
        .pipe(plugins.plumber({
            errorHandler: function (error) {
                console.log('Scripts Error: ' + error.message);
                this.emit('end');
            }
        }))
        .pipe(sourcemaps.init())
        .pipe(plugins.concat('./scripts.min.js'))
        .pipe(plugins.uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(paths.js.dest));
});

gulp.task('images', function() {
    gulp.src([paths.images.src + '/**/*'])
        .pipe(plugins.plumber({
            errorHandler: function (error) {
                console.log('Images Error: ' + error.message);
                this.emit('end');
            }
        }))
        .pipe(plugins.changed(paths.images.dest))
        .pipe(plugins.imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
        .pipe(gulp.dest(paths.images.dest));
});

gulp.task('default', ['styles', 'scripts', 'images'], function () {
    gulp.watch(paths.css.src + '/**/*.scss', ['styles']);
    gulp.watch(paths.js.src + '/**/*.js', ['scripts']);
    gulp.watch(paths.images.src + '/**/*', ['images']);
});