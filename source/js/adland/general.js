$(function() {
    var $info = $('#info'),
        $infoTab = $('#info-tab');

    $infoTab.on('click', function() {
        if($infoTab.hasClass('open')) {
            $info.removeClass('open');
            $infoTab.removeClass('open');
        } else {
            $info.addClass('open');
            $infoTab.addClass('open');
        }
    });

    if(!Modernizr.svg) {
        $('img[src*="svg"]').attr('src', function() {
            return $(this).attr('src').replace('.svg', '.png');
        });
    }
});