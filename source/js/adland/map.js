$(function() {
    google.maps.event.addDomListener(window, 'load', initialize);

    function initialize() {
        var locations = [
            {
                latlng: new google.maps.LatLng(51.519333, -0.16155300000002626),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'Julie @ Duke of Wellington',
                name: 'AMV BBDO',
                text: "The Duke of Wellington has long served the thirsty ad people of AMV BBDO.<br /><br />Tell us why Julie is so great and should be voted Adland-Lord 2015 by dropping us an email at <a href=\"mailto:adlandlords@theredbrickroad.com\">adlandlords@theredbrickroad.com</a>",
                content: '<img src="img/pictures/amv-bbdo.jpg" alt="Julie @ Duke of Wellington" />',
                silhouette: true,
                logo: 'img/logos/amv-bbdo.png'
            },
            {
                latlng: new google.maps.LatLng(51.52307, -0.10929999999996198),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'Giles @ Coach and Horses',
                name: 'Anomaly',
                text: "One of the more traditional pub names on our list, The Coach and Horses is Anomaly's London base.<br /><br />Should Giles be voted Adland-Lord 2015? Tell us why at <a href=\"mailto:adlandlords@theredbrickroad.com\">adlandlords@theredbrickroad.com</a>",
                content: '<img src="img/pictures/anomaly.jpg" alt="The Coach and Horses" />',
                silhouette: true,
                logo: 'img/logos/anomaly.png'
            },
            {
                latlng: new google.maps.LatLng(51.511672, -0.1256750000000011),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'John @ The Lamb and Flag',
                name: 'BMB',
                text: "Tucked away down an alleyway, the secretive Lamb and Flag serves up beers for BMB.<br /><br />Tell us why John is so great and should be voted Adland-Lord 2015 by dropping us an email at <a href=\"mailto:adlandlords@theredbrickroad.com\">adlandlords@theredbrickroad.com</a>",
                content: '<img src="img/pictures/bmb.jpg" alt="John @ The Lamb and Flag" />',
                silhouette: true,
                logo: 'img/logos/bmb.png'
            },
            {
                latlng: new google.maps.LatLng(51.52476739999999, -0.14266240000006292),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'Bart @ Union Bar',
                name: 'Carat',
                text: "Carat's Union Bar is a welcome respite from the hustle and bustle of Marylebone Road.<br /><br />Should Bart be voted Adland-Lord 2015? Tell us why at <a href=\"mailto:adlandlords@theredbrickroad.com\">adlandlords@theredbrickroad.com</a>",
                content: '<img src="img/pictures/carat.jpg" alt="Bart @ Union Bar" />',
                silhouette: true,
                logo: 'img/logos/carat.png'
            },
            {
                latlng: new google.maps.LatLng(51.518207, -0.135206),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'Debi @ The Duke of York',
                name: 'CHI &amp; Partners',
                text: "A stone's throw from UCL, CHI & Partners are spoiled for choice amongst pubs, but remain loyal to The Duke of York.<br /><br />Should Debi be voted Adland-Lord 2015? Tell us why at <a href=\"mailto:adlandlords@theredbrickroad.com\">adlandlords@theredbrickroad.com</a>",
                content: '<img src="img/pictures/chi.jpg" alt="Debi @ The Duke of York" />',
                silhouette: true,
                logo: 'img/logos/chi.png'
            },
            {
                latlng: new google.maps.LatLng(51.522892, -0.08049700000003668),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'Christian @ The Horse and Groom',
                name: 'Creature',
                text: "What more appropriate pub for Creature to drink at that Curtain Road's The Horse and Groom?<br /><br />Tell us why Christian is so great and should be voted Adland-Lord 2015 by dropping us an email at <a href=\"mailto:adlandlords@theredbrickroad.com\">adlandlords@theredbrickroad.com</a>",
                content: '<img src="img/pictures/creature.jpg" alt="Christian @ The Horse and Groom" />',
                silhouette: false,
                logo: 'img/logos/creature.png'
            },
            {
                latlng: new google.maps.LatLng(51.516097, -0.12415199999998094),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'Gia @ The White Hart',
                name: 'Critical Mass',
                text: "It looks small and unassuming from the outside, but Critical Mass's The White Hart is surprisingly big inside.<br/>\"Week-long detox\" apparently doesn't mean the same for Critical Mass as it does for most people...",
                content: '<iframe width="100%" height="auto" src="https://www.youtube.com/embed/SX57vsFUs9U?rel=0" frameborder="0" allowfullscreen></iframe>',
                silhouette: false,
                logo: 'img/logos/critical-mass.png'
            },
            {
                latlng: new google.maps.LatLng(51.49366400000001, -0.16605600000002596),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'Siobhain @ The Admiral Codrington',
                name: 'DLKW Lowe',
                text: "Almost certainly the only pub on this list with a sun roof, it's no surprise DLKW Lowe choose to drink at The Admiral Codrington.<br /><br />Does Siobhain deserve to be crowned Adland-Lord 2015? Give us your reasons via <a href=\"mailto:adlandlords@theredbrickroad.com\">adlandlords@theredbrickroad.com</a>",
                content: '<img src="img/pictures/dlkw-lowe.jpg" alt="Siobhain @ The Admiral Codrington" />',
                silhouette: true,
                logo: 'img/logos/dlkw-lowe.png'
            },
            {
                latlng: new google.maps.LatLng(51.497290, -0.137019),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'Paola @ The Albert',
                name: 'Droga5',
                text: "Droga5's recently acquired offices have led them directly to the door of their new pub; The Albert.<br /><br />Should Paola be voted Adland-Lord 2015? Tell us why at <a href=\"mailto:adlandlords@theredbrickroad.com\">adlandlords@theredbrickroad.com</a>",
                content: '<img src="img/pictures/droga5.jpg" alt="Paola @ The Albert" />',
                silhouette: true,
                logo: 'img/logos/droga5.png'
            },
            {
                latlng: new google.maps.LatLng(51.51935700000001, -0.1406819999999698),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'Garry @ The Crown and Sceptre',
                name: 'Elvis Comms',
                text: "Fittingly, for an agency named after The King, Elvis Comms drink at The Crown and Sceptre.<br /><br />Tell us why Garry is so great and should be voted Adland-Lord 2015 by dropping us an email at <a href=\"mailto:adlandlords@theredbrickroad.com\">adlandlords@theredbrickroad.com</a>",
                content: '<img src="img/pictures/elvis-comms.jpg" alt="Garry @ The Crown and Sceptre" />',
                silhouette: true,
                logo: 'img/logos/elvis-comms.png'
            },
            {
                latlng: new google.maps.LatLng(51.513392, -0.13050899999996093),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'Tom @ Soho House',
                name: 'Fallon',
                text: "Bringing a touch of class to our map are Fallon, with their watering hole of choice being Soho House. Of course.<br /><br />Does Tom deserve to be crowned Adland-Lord 2015? Give us your reasons via <a href=\"mailto:adlandlords@theredbrickroad.com\">adlandlords@theredbrickroad.com</a>",
                content: '<img src="img/pictures/fallon.jpg" alt="Tom @ Soho House" />',
                silhouette: true,
                logo: 'img/logos/fallon.png'
            },
            {
                latlng: new google.maps.LatLng(51.52319, -0.13866400000006252),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'Debi @ The Grafton Arms',
                name: 'FKC',
                text: "FKC - The Grafton Arms. Straight to the point.<br /><br />Should Debi be voted Adland-Lord 2015? Tell us why at <a href=\"mailto:adlandlords@theredbrickroad.com\">adlandlords@theredbrickroad.com</a>",
                content: '<img src="img/pictures/fkc.jpg" alt="Debi @ The Grafton Arms" />',
                silhouette: true,
                logo: 'img/logos/fkc.png'
            },
            {
                latlng: new google.maps.LatLng(51.521680, -0.103787),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'David @ The Jerusalem Tavern',
                name: 'Fold7',
                text: "Possibly the oldest pub on this list, representing the very modern Fold7 is The Jerusalem Tavern.<br /><br />Should David be voted Adland-Lord 2015? Tell us why at <a href=\"mailto:adlandlords@theredbrickroad.com\">adlandlords@theredbrickroad.com</a>",
                content: '<img src="img/pictures/fold7.jpg" alt="David @ The Jerusalem Tavern" />',
                silhouette: true,
                logo: 'img/logos/fold7.png'
            },
            {
                latlng: new google.maps.LatLng(51.521811, -0.10811599999999545),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'Ljupco @ Hat and Tun',
                name: 'Grey',
                text: "A proper old-fashioned rub-a-dub in the centre of London. No wonder Grey choose to drink at the Hat and Tun.<br /><br />Grey - they're there every lunchtime, and they refuse to sit on the chairs... Find out more by watching the video",
                content: '<iframe width="100%" height="auto" src="https://www.youtube.com/embed/s-LJZ-otvb8?rel=0" frameborder="0" allowfullscreen></iframe>',
                silhouette: false,
                logo: 'img/logos/grey.png'
            },
            {
                latlng: new google.maps.LatLng(51.519799, -0.1318340000000262),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'Steve @ The College Arms',
                name: 'Leagas Delaney',
                text: "The old-school (pun intended) stylings of The College Arms are home to the ad people from Leagas Delaney.<br /><br />Should Steve be voted Adland-Lord 2015? Tell us why at <a href=\"mailto:adlandlords@theredbrickroad.com\">adlandlords@theredbrickroad.com</a>",
                content: '<img src="img/pictures/leagas-delaney.jpg" alt="Steve @ The College Arms" />',
                silhouette: true,
                logo: 'img/logos/leagas-delaney.png'
            },
            {
                latlng: new google.maps.LatLng(51.512536, -0.13737200000002758),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'Brodie @ Old Coffee House',
                name: 'M&C Saatchi',
                text: "Combining the twin loves of advertising people everywhere; coffee and alcohol, The Old Coffee House is a double whammy for M&C Saatchi.<br /><br />Tell us why Brodie is so great and should be voted Adland-Lord 2015 by dropping us an email at <a href=\"mailto:adlandlords@theredbrickroad.com\">adlandlords@theredbrickroad.com</a>",
                content: '<img src="img/pictures/mc-saatchi.jpg" alt="Brodie @ Old Coffee House" />',
                silhouette: false,
                logo: 'img/logos/mc-saatchi.png'
            },
            {
                latlng: new google.maps.LatLng(51.524387000000004, -0.1249679999999671),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'Susanna @ Marquis Cornwallis',
                name: 'McCann',
                text: "Marquis Cornwallis. McCann London. Even the names sound alike.<br /><br />Does Susanna deserve to be crowned Adland-Lord 2015? Give us your reasons via <a href=\"mailto:adlandlords@theredbrickroad.com\">adlandlords@theredbrickroad.com</a>",
                content: '<img src="img/pictures/mccan.jpg" alt="Susanna @ Marquis Cornwallis" />',
                silhouette: true,
                logo: 'img/logos/mccan.png'
            },
            {
                latlng: new google.maps.LatLng(51.524283, -0.0754660000000058),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'Matt @ The Owl and the Pussycat',
                name: 'Mother',
                text: "The Owl and the Pussycat went to sea... Ok they didn't, but Mother London went to the Owl and Pussycat in Shoreditch.<br /><br />The Owl and the Pussycat couldn't go into too much detail as then they be forced to involve the authorities... Find out more in the video...",
                content: '<iframe width="100%" height="auto" src="https://www.youtube.com/embed/yvL-3p8QsRk?rel=0" frameborder="0" allowfullscreen></iframe>',
                silhouette: false,
                logo: 'img/logos/mother.png'
            },
            {
                latlng: new google.maps.LatLng(51.494112, -0.1821989999999687),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'Sonila @ Stanhope Arms',
                name: 'Nexus',
                text: "The most So'Westerly agency on our list - Nexus call The Stanhope Arms their local.<br /><br />Should Sonila be voted Adland-Lord 2015? Tell us why at <a href=\"mailto:adlandlords@theredbrickroad.com\">adlandlords@theredbrickroad.com</a>",
                content: '<img src="img/pictures/nexus.jpg" alt="Sonila @ Stanhope Arms" />',
                silhouette: true,
                logo: 'img/logos/nexus.png'
            },
            {
                latlng: new google.maps.LatLng(51.506103, -0.02204200000005585),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'James @ The Cat and Canary',
                name: 'Ogilvy',
                text: "Canary Wharf is the far-flung home of Ogilvy London, who drink in the Cat and Canary.<br /><br />Tell us why James is so great and should be voted Adland-Lord 2015 by dropping us an email at <a href=\"mailto:adlandlords@theredbrickroad.com\">adlandlords@theredbrickroad.com</a>",
                content: '<img src="img/pictures/ogilvy.jpg" alt="James @ The Cat and Canary" />',
                silhouette: true,
                logo: 'img/logos/ogilvy.png'
            },
            {
                latlng: new google.maps.LatLng(51.517931875745695, -0.13192913226316705),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'Paulo @ The Nellie Dean',
                name: 'Publicis',
                text: "Although they've recently moved office, the Nellie Dean will always be (spiritually at least) the home of Publicis.<br /><br />Watch the video to find out why Publicis were dancing on the tables...",
                content: '<iframe width="100%" height="auto" src="https://www.youtube.com/embed/8OiG9GYdMkI?rel=0" frameborder="0" allowfullscreen></iframe>',
                silhouette: false,
                logo: 'img/logos/publicis.png'
            },
            {
                latlng: new google.maps.LatLng(51.535147, -0.1416910000000371),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'Sarah @ The Sheephaven Bay',
                name: 'RKCR',
                text: "After the Victoria was taken away from them, RKCR have decamped round the corner to worthy replacement the Sheephaven Bay.<br /><br />120 people in one small Camden pub? You better believe it - find out what went on in the video...",
                content: '<iframe width="100%" height="auto" src="https://www.youtube.com/embed/K0-huBELZlM?rel=0" frameborder="0" allowfullscreen></iframe>',
                silhouette: false,
                logo: 'img/logos/rkcr-yr.png'
            },
            {
                latlng: new google.maps.LatLng(51.521808, -0.13652799999999843),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'Daniel @ The Carpenter\'s Arms',
                name: 'Saatchi & Saatchi',
                text: "When not kicking about in The Pregnant Man, Saatchi & Saatchi tend to hit up the Carpenter's Arms.<br /><br />Does Daniel deserve to be crowned Adland-Lord 2015? Give us your reasons via <a href=\"mailto:adlandlords@theredbrickroad.com\">adlandlords@theredbrickroad.com</a>",
                content: '<img src="img/pictures/saatchi-saatchi.jpg" alt="Daniel @ The Carpenter\'s Arms" />',
                silhouette: true,
                logo: 'img/logos/saatchi-saatchi.png'
            },
            {
                latlng: new google.maps.LatLng(51.513841, -0.135006),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'Bill @ The Blue Posts',
                name: 'The Red Brick Road',
                text: "The Red Brick Road's first port of call for any agency booze-up, one of Soho's finest; The Blue Posts.<br /><br />Make sure you watch the video to see what our favourite Landlord thinks.",
                content: '<iframe width="100%" height="auto" src="https://www.youtube.com/embed/hCzjTG8-BzE?rel=0" frameborder="0" allowfullscreen></iframe>',
                silhouette: false,
                logo: 'img/redbrick-logo.svg'
            },
            {
                latlng: new google.maps.LatLng(51.496301, -0.13579200000003766),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'Grant @ The Greencoat Boy',
                name: 'VCCP',
                text: "VCCP's haunt in on Greencoat Place - the aptly-named Greencoat Boy.<br /><br />Tell us why Grant is so great and should be voted Adland-Lord 2015 by dropping us an email at <a href=\"mailto:adlandlords@theredbrickroad.com\">adlandlords@theredbrickroad.com</a>",
                content: '<img src="img/pictures/vccp.jpg" alt="Grant @ The Greencoat Boy" />',
                silhouette: true,
                logo: 'img/logos/vccp.png'
            },
            {
                latlng: new google.maps.LatLng(51.519367, -0.07428700000002664),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'John @ The Ten Bells',
                name: 'Wieden + Kennedy',
                text: "Once a lawyer's hang-out, The Ten Bells is now dominated by Wieden + Kennedy.<br /><br />Should John be voted Adland-Lord 2015? Tell us why at <a href=\"mailto:adlandlords@theredbrickroad.com\">adlandlords@theredbrickroad.com</a>",
                content: '<img src="img/pictures/wieden-kennedy.jpg" alt="John @ The Ten Bells" />',
                silhouette: true,
                logo: 'img/logos/wieden-kennedy.png'
            },
            {
                latlng: new google.maps.LatLng(51.518873, -0.13227699999993092),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'Invra @ The Jack Horner',
                name: 'Zenith Optimedia',
                text: "Right on the corner of Tottenham Court Road is The Jack Horner - favourite of all at Zenith Optimedia.<br /><br />Tell us why Invra is so great and should be voted Adland-Lord 2015 by dropping us an email at <a href=\"mailto:adlandlords@theredbrickroad.com\">adlandlords@theredbrickroad.com</a>",
                content: '<img src="img/pictures/zenith-optimedia.jpg" alt="Invra @ The Jack Horner" />',
                silhouette: true,
                logo: 'img/logos/zenith-optimedia.png'
            },
            {
                latlng: new google.maps.LatLng(51.512619, -0.13663399999995818),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'Pascal @ The Sun and 13 Cantons',
                name: 'Quiet Storm',
                text: "Luckily - they're located practically in the same building, which is why Quiet Storm love the Sun & 13.<br /><br />Does Pascal deserve to be crowned Adland-Lord 2015? Give us your reasons via <a href=\"mailto:adlandlords@theredbrickroad.com\">adlandlords@theredbrickroad.com</a>",
                content: '<img src="img/pictures/quiet-storm.jpg" alt="Pascal @ The Sun and 13 Cantons" />',
                silhouette: true,
                logo: 'img/logos/quiet-storm.png'
            },
            {
                latlng: new google.maps.LatLng(51.501109, -0.1624970000000303),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'Vanda @ Tattersalls Tavern',
                name: 'JWT',
                text: "JWT love their green space, that's why their pub - Tattersalls Tavern - is right next to Knightsbridge Green.<br /><br />Does Vanda deserve to be crowned Adland-Lord 2015? Give us your reasons via <a href=\"mailto:adlandlords@theredbrickroad.com\">adlandlords@theredbrickroad.com</a>",
                content: '<img src="img/pictures/jwt.jpg" alt="Vanda @ Tattersalls Tavern" />',
                silhouette: true,
                logo: 'img/logos/jwt.png'
            },
            {
                latlng: new google.maps.LatLng(51.531611, -0.12242700000001605),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'Samantha @ The Fellow',
                name: 'Cheil',
                text: "Cheil and The Fellow are a match made in heaven.<br /><br />Does Samantha deserve to be crowned Adland-Lord 2015? Give us your reasons via <a href=\"mailto:adlandlords@theredbrickroad.com\">adlandlords@theredbrickroad.com</a>",
                content: '<img src="img/pictures/cheil.jpg" alt="The Fellow" />',
                silhouette: true,
                logo: 'img/logos/cheil.png'
            },
            {
                latlng: new google.maps.LatLng(51.525724, -0.14414399999998295),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'David @ Queen\'s Head and Artichoke',
                name: 'Aegis Media',
                text: "The fancy Queen's Head and Artichoke is the pub of choice for Aegis Media.<br /><br />Does David deserve to be crowned Adland-Lord 2015? Give us your reasons via <a href=\"mailto:adlandlords@theredbrickroad.com\">adlandlords@theredbrickroad.com</a>",
                content: '<img src="img/pictures/aegis-media.jpg" alt="David @ Queen\'s Head and Artichoke" />',
                silhouette: true,
                logo: 'img/logos/aegis-media.png'
            },
            {
                latlng: new google.maps.LatLng(51.51728000000001, -0.18018800000004376),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'Peter @ The Prince of Wales',
                name: 'Adam & Eve DDB',
                text: "Although they're not exactly spoilt for choice out in West London, The Prince of Wales more than does the job for Adam & Eve DDB.<br /><br />This one really has to be seen to be believed. Watch the video to find out why...",
                content: '<iframe width="100%" height="auto" src="https://www.youtube.com/embed/ks-bLWs6D2o?rel=0" frameborder="0" allowfullscreen></iframe>',
                silhouette: false,
                logo: 'img/logos/adam-eve-ddb.png'
            },
            {
                latlng: new google.maps.LatLng(51.524269, -0.13854000000003455),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'Matthew @ The Prince of Wales Feathers',
                name: '18 Feet and Rising',
                text: "Right next to their tube station, the Prince of Wales Feathers is a convenient little boozer for 18 Feet & Rising.<br/>Tell us why Matthew is so great and should be voted Adland-Lord 2015 by dropping us an email at <a href=\"mailto:adlandlords@theredbrickroad.com\">adlandlords@theredbrickroad.com</a>",
                content: '<img src="img/pictures/18-feet-and-rising.jpg" alt="Matthew @ The Prince of Wales Feathers" />',
                silhouette: true,
                logo: 'img/logos/18-feet-and-rising.png'
            },
            {
                latlng: new google.maps.LatLng(51.512828, -0.13946399999997539),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'Lucas @ The Blue Posts',
                name: 'BBH',
                text: "Conveniently located directly opposite their offices, The Blue Posts has a long-standing relationship with BBH.<br /><br />Does Lucas deserve to be crowned Adland-Lord 2015? Give us your reasons via <a href=\"mailto:adlandlords@theredbrickroad.com\">adlandlords@theredbrickroad.com</a>",
                content: '<img src="img/pictures/bbh.jpg" alt="Lucas @ The Blue Posts" />',
                silhouette: true,
                logo: 'img/logos/bbh.png'
            },
            {
                latlng: new google.maps.LatLng(51.520408, -0.13530900000000656),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'Phil @ The Hope',
                name: 'Rocket Network',
                text: "Nestled just behind Goodge Street station is Rocket Network's boozer - The Hope.<br /><br />Should Phil be voted Adland-Lord 2015? Tell us why at <a href=\"mailto:adlandlords@theredbrickroad.com\">adlandlords@theredbrickroad.com</a>",
                content: '<img src="img/pictures/rocket-network.jpg" alt="Phil @ The Hope" />',
                silhouette: true,
                logo: 'img/logos/rocket-network.png'
            },
            {
                latlng: new google.maps.LatLng(51.510978, -0.12696900000003097),
                icon: 'img/map-pint-pin.svg',
                group: 'pub',
                title: 'John @ The Angel and Crown',
                name: 'Havas',
                text: "Recently refurbed, Havas will be glad to have their old haunt the Angel and Crown back.<br /><br />Tell us why John is so great and should be voted Adland-Lord 2015 by dropping us an email at <a href=\"mailto:adlandlords@theredbrickroad.com\">adlandlords@theredbrickroad.com</a>",
                content: '<img src="img/pictures/havas.jpg" alt="John @ The Angel and Crown" />',
                silhouette: true,
                logo: 'img/logos/havas.png'
            },
            {
                latlng: new google.maps.LatLng(51.504894, -0.18813000000000102),
                icon: 'img/map-pin.svg',
                group: 'event',
                title: 'The Orangery at Kensington Palace',
                name: 'AWE Venue',
                text: '',
                content: '',
                silhouette: false
            },
            {
                latlng: new google.maps.LatLng(51.499047, -0.12486100000000988),
                icon: 'img/map-pin.svg',
                group: 'event',
                title: 'House of Lords',
                name: 'AWE Venue',
                text: '',
                content: '',
                silhouette: false
            },
            {
                latlng: new google.maps.LatLng(51.504586, -0.1378270000000157),
                icon: 'img/map-pin.svg',
                group: 'event',
                title: 'St James\'s Palace',
                name: 'AWE Venue',
                text: '',
                content: '',
                silhouette: false
            },
            {
                latlng: new google.maps.LatLng(51.5088711, -0.1366562000000613),
                icon: 'img/map-pin.svg',
                group: 'event',
                title: 'News Room Studio B',
                name: 'AWE Venue',
                text: '',
                content: '',
                silhouette: false
            },
            {
                latlng: new google.maps.LatLng(51.50896729999999, -0.13652700000000095),
                icon: 'img/map-pin.svg',
                group: 'event',
                title: 'Channel 6 Stage @ the #AWEurope Underground',
                name: 'AWE Venue',
                text: '',
                content: '',
                silhouette: false
            },
            {
                latlng: new google.maps.LatLng(51.513432, -0.13153799999997773),
                icon: 'img/map-pin.svg',
                group: 'event',
                title: 'Ronnie Scott\'s Jazz Club',
                name: 'AWE Venue',
                text: '',
                content: '',
                silhouette: false
            },
            {
                latlng: new google.maps.LatLng(51.5183671681425, -0.12193053960800172),
                icon: 'img/map-pin.svg',
                group: 'event',
                title: 'Lancaster House',
                name: 'AWE Venue',
                text: '',
                content: '',
                silhouette: false
            },
            {
                latlng: new google.maps.LatLng(51.53077, -0.12082199999997555),
                icon: 'img/map-pin.svg',
                group: 'event',
                title: 'Scala',
                name: 'AWE Venue',
                text: '',
                content: '',
                silhouette: false
            },
            {
                latlng: new google.maps.LatLng(51.53472, -0.13838899999996102),
                icon: 'img/map-pin.svg',
                group: 'event',
                title: 'Koko',
                name: 'AWE Venue',
                text: '',
                content: '',
                silhouette: false
            }
        ],
        mapOptions = {
            center: new google.maps.LatLng(51.507351, -0.127758),
            zoom: 13,
            zoomControl: false,
            panControl: false,
            streetViewControl: false,
            overviewMapControl: false,
            mapTypeControl: false,
            navigationControl: false,
            scaleControl: false,
            styles: [
                {
                    "featureType": "road",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#ff2302"
                        },
                        {
                            "weight": 0.5
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#808080"
                        }
                    ]
                },
                {
                    "featureType": "landscape.natural",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "hue": "#ff0022"
                        },
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#ff131f"
                        },
                        {
                            "saturation": 12
                        },
                        {
                            "lightness": 48
                        }
                    ]
                },
                {
                    "featureType": "landscape.man_made",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#ff1b2a"
                        },
                        {
                            "hue": "#ff0008"
                        },
                        {
                            "lightness": 74
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#ff1229"
                        },
                        {
                            "saturation": 15
                        },
                        {
                            "lightness": 52
                        }
                    ]
                },
                {
                    featureType: "poi",
                    elementType: "labels",
                    stylers: [
                        {
                            visibility: "off"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "stylers": [
                        {
                            "color": "#3da5ff"
                        },
                        {
                            "saturation": 16
                        },
                        {
                            "lightness": 51
                        }
                    ]
                },
                {
                    "featureType": "transit.line",
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 100
                        }
                    ]
                }
            ]
        },
        map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions),
        bounds = new google.maps.LatLngBounds(),
        infoBoxOptions = {
            maxWidth: 400,
            pixelOffset: new google.maps.Size(45, 0),
            boxStyle: {
                background: "#ECECEB",
                opacity: 1,
                width: "300px",
                borderRadius: "10px"
            },
            closeBoxMargin: "-7px -7px 0 0",
            closeBoxURL: Modernizr.svg ? "img/info-close.svg" : "img/info-close.png",
            infoBoxClearance: new google.maps.Size(10, 10),
            pane: "floatPane",
            enableEventPropagation: true
        },
        infoBox = new InfoBox(infoBoxOptions);

        google.maps.event.addDomListener(window, "resize", function() {
            google.maps.event.trigger(map, "resize");
            map.setCenter(map.getCenter());
            map.fitBounds(bounds);
        });

        $.each(locations, function(index, location) {
            var iconUrl = location.icon,
                locationLogo = location.logo;
                locationContent = location.content;

            if(!Modernizr.svg || /Trident\/7\./.test(navigator.userAgent)) {
                iconUrl = iconUrl.replace('.svg', '.png');

                if(!Modernizr.svg) {
                    locationLogo = locationLogo.replace('.svg', '.png');
                }
            }

            var icon = {
                url: iconUrl,
                origin: new google.maps.Point(0, 0)
            };

            if(location.group === 'event') {
                icon.size = new google.maps.Size(30, 46);
                icon.anchor = new google.maps.Point(15, 46);
            } else if(location.group === 'pub') {
                icon.size = new google.maps.Size(30, 38);
                icon.anchor = new google.maps.Point(15, 19);
            }

            if(location.silhouette) {
                locationContent += '<img class="silhouette" src="img/silhouette.png" alt="Silhouette" />';
            }

            var content = '<div class="gm-style-iw ' + location.group + '"><p class="arrow"></p>';
            content += '<div class="content-wrapper">' + locationContent + "</div>";
            content += '<h2>' + location.title + '</h2>';

            var voteBtnClass = typeof $.cookie('voted') !== 'undefined' ? ' disabled' : '';

            if(location.group === 'pub') {
                content += '<h3>Local Agency: ' + location.name + '</h3>';
                content += '<p>' + location.text + '</p>';
                content += '<div class="vote-wrapper">' +
                    '<img class="pub-logo" src="' + locationLogo +'" alt="' + location.title + '" />' +
                    '<span class="vote-btn' + voteBtnClass + '" data-location="' + index + '">Vote</span>' +
                '</div>';
            }

            content += '</div>';

            var marker = new google.maps.Marker({
                position: location.latlng,
                map: map,
                icon: icon,
                content: content,
                group: location.group
            });

            bounds.extend(location.latlng);
            map.fitBounds(bounds);

            google.maps.event.addListener(marker, "click", function() {
                infoBox.setContent(this.content);

                if(this.group === 'pub') {
                    infoBox.setOptions({
                        pixelOffset: new google.maps.Size(45, 0)
                    });
                } else {
                    infoBox.setOptions({
                        pixelOffset: new google.maps.Size(50, -20)
                    });
                }

                infoBox.open(map, this);
            });
        });

        $(document).on('click', '.vote-btn', function() {
            var $this = $(this);

            if(typeof $.cookie('voted') !== 'undefined' || $this.hasClass('disabled')) {
                $('body').addClass('voted');
                $this.addClass('disabled');
                return false;
            }

            $.cookie('voted', true);

            $.ajax({
                url: 'php/vote.php',
                type: 'post',
                data: {
                    location: locations[$(this).data('location')].title
                }
            }).done(function() {
                $('body').addClass('voted');
                $this.addClass('disabled');
            });
        });
    }
});