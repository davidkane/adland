<?php

$response = array(
    'status' => 'fail',
    'data' => 'Invalid access'
);

if( isset( $_POST['location'] ) ) {
    $dbconfig = require_once '../../config/db.php';

    try {
        $db = new PDO('mysql:host=' . $dbconfig['host'] . ';dbname=' . $dbconfig['dbname'], $dbconfig['username'], $dbconfig['password']);

        $sql = "INSERT INTO votes (pub, votes) VALUES (:pub, :votes) ON DUPLICATE KEY UPDATE votes = votes + 1";
        $pub = (string) $_POST['location'];
        $count = 1;

        $query = $db->prepare($sql);
        $query->bindParam(':pub', $pub, PDO::PARAM_STR);
        $query->bindParam(':votes', $count, PDO::PARAM_INT);
        $query->execute();
        $db = null;

        $response = array(
            'status' => 'success',
            'data' => array()
        );
    } catch (PDOException $e) {
        $response['data'] = $e->getMessage();
    }
}

header('Content-Type: application/json');
echo json_encode($response);