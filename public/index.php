<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
        <meta charset="utf-8">
        <title>AdLand-Lord Award 2015</title>
        <link href="css/style.css" type="text/css" rel="stylesheet" />
        <script src="js/modernizr.min.js" type="text/javascript"></script>
    </head>
    <body>
        <div id="header">
            <img class="redbrick-logo" src="img/redbrick-logo.svg" alt="The Red Brick Road" />
            <p>Vote for your AdLand-Lord 2015</p>
            <img class="adland-logo" src="img/adland-logo.svg" alt="AdLand-Lord" />
        </div>
        <div id="info-tab" class="open">
            <img src="img/info-tab.svg" alt="Information" />
        </div>
        <div id="info" class="open">
            <h2>Adland-Lords</h2>
            <p>Welcome to Adland, where Creatives converse and Suits swap stories about encounters of the client kind.</p>
            <p>To help you navigate your way through it, here's a handy map of the Adland HQs - the pubs that house us and the landlords who put up with us all.</p>
            <p>They've been there with us through the pitch wins and the Christmas parties, pandered to our 'Thirsty Thursdays' and plied us with hair of the dog on Fridays.</p>
            <p>So maybe it's time to show our Adland-Lords a little love in return.</p>
            <p>Click on the links to see what these long-standing patrons of the advertising industry have to say about us, then vote for your favourite.</p>
            <p>
                <a href="http://twitter.com/intent/tweet?status=%23AdlandLords" target="_blank" onclick="javascript:window.open(this.href,
  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=252,width=700');return false;">
                    <img src="img/twitter.png" alt="Twitter" />
                    <span>#AdlandLords</span>
                </a>
            </p>
            <img class="key" src="img/key.svg" alt="Map Key" />
        </div>
        <div id="key">
            <img class="key" src="img/key.svg" alt="Map Key" />
        </div>
        <div id="map-canvas"></div>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.20" type="text/javascript"></script>
        <script src="js/scripts.min.js" type="text/javascript"></script>
    </body>
</html>